 const AmountElements = 9;
       MinValueRange = -15;
       MaxValueRange = 13;

 type numbers = array [1..AmountElements][1..AmountElements] of Integer;

 procedure CreatesMatrix (var randomNumbers: numbers);
 var i, j : Integer;
 begin
     for i := 1 to AmountElements do
     begin
         for j := 1 to AmountElements do
         begin
             randomNumbers[i,j] := Random(MaxValueRange - MinValueRange + 1) - MaxValueRange;
         end;
     end;
 end;

 procedure DisplaysArray (outputNumbers: numbers);
 var i, j : Integer;
 begin
     for i := 1 to AmountElements do
     begin
         for j := 1 to AmountElements do 
         begin
             Write(outputNumbers[i,j],' ');
         end;
         WriteLn;
     end;
 end;

 function SearchesNumberNegativeAnItem (numbersToSearch: numbers) :    Integer;
 var i : Integer;
 begin
     SearchesNumberNegativeAnItem := 0;
     for i := 1 to AmountElements do
     begin
         if numbersToSearch[i,i] < 0 then
         begin
             SearchesNumberNegativeAnItem++;
         end;
     end;
 end;

 procedure GhangeEllement (numbersGhange: numbers);
 var i, j : Integer;
 begin
     for i := 1 to AmountElements do
     begin
         for j := 1 to AmountElements do 
         begin
             if j-i = 1 then
             begin
                 if numbersGhange[i,j] > 0 then
                 begin
                     numbersGhange[i,j] := 0;
                 end;
             end;
         end;
     end;
 end.

 var mainMatrix: numbers;
 begin
 Randomize;
 CreatesMatrix (mainMatrix);
 WriteLn ('Matrix:');
 DisplaysArray (mainMatrix);
 ReadLn;
 WriteLn ('The number of negative elements on the main diagonal: ', SearchesNumberNegativeAnItem(mainMatrix));
 ReadLn;
 GhangeEllement (mainMatrix);
 WriteLn ('The modified matrix:');
 DisplaysArray (mainMatrix);
 ReadLn;
 end.